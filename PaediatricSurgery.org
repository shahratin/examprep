* Rectal bleeding
** Rectal polyp
- *Definition:* Rectal polyp is projecting mass into the lumen of bowel above the surface of epithelium of rectum.
- *Causes:*
*** Juvenile polyps
- Juvenile polyps are the most common type of hamartomatous polyp
- Juvenile polyps are focal malformations of the mucosal epithelium and lamina propria
- These may be sporadic or syndromic
- The vast majority of juvenile polyps occur in children less than 5 years of age
- The majority of juvenile polyps are located in the rectum and most present with rectal bleeding. In some cases prolaps occurs and the polyp protrudes through the anal sphicter.
- Sporadic juvenile polyps are usually solitary lesions and may be referred to as retention polyps.
- In contrast, individuals with the autosomal dominant syndrome of jevenile polyposis have from 3 to as many as 100 hamartomatous polyps and may require colectomy to limit the chronic and sometimes severe haemorrhage associated with polyp ulceration.
*** Inherited hamartomatous polyposis syndromes
**** Peutz-Jeghers syndrome
**** Juvenile polyposis
**** Cowden's syndrome
*** Inherited adenomatous polyposis syndromes
**** Familial polyposis coli
**** Gardner's syndrome
**** Turcot's syndrome
*** Noninherited polyposis
**** Lymphoid polyposis
** Infectious colitis
** Ulcerative colitis
** Non specific colitis
** Intussusception
** Anal fissure
** Meckel's diverticulum
- *Definition:* It is a congenital diverticulum arising from the antimesenteric border of terminal ileum
- *Cause:* Failure to incomplete obliteration of vitelline duct.
- If the Meckel's Diverticulum is found in an inguinal or femoral sac it's called Littre's hernia.
- *Epidemiology:*
  + 2% of the general population
  + 2:1 male predominance
  + 2 feet proximal to the ileocecal valve in adults
  + 50% symptomatic under 2 years
  + 2 inches long
  + In adult patients it is symptomatic in only about 2%
  + Heterotropic tissue (can be 2 types)
    - Gastric mucosa
    - Pancreatic acini
  + *Sequele:*
    - Severe haemorrhage
    - Intussusception
    - Meckel's diverticulitis
    - Chronic peptic ulceration
    - Intestinal obstruction
  + *Cause of severe haemorrhage:*
    - Painless per rectal bleeding, maroon coloured haemorrhage
    - Ectopic gastric or pancreatic mucosa
    - Secretion of gastric acid or alkaline pancreatic juice from the ectopic mucosa leads to ulceration in the adjacent ileal mucosa
    - Perforation and bleeding from the ulcer
  + *Cause of Meckel's diverticulits:*
    - Peptic ulceration
    - Perforation by trauma or ingested food residue
    - Luminal obstruction due to tumour, foreign body, causing stais or bacterial infection
  + *Cause of Intestinal Obstruction:*
    - Volvulus of the intestine
    - Entrapment of the intestine by a mesodiverticular band
    - Intussusception with the diverticulum
    - Stricture secondary to chronic diverticulitis
  + *Indication for surgery:*
    - Symptomatic meckel's diverticulum:
      + Haemorrhage
      + Intestinal obstruction
      + Diverticulitis
      + Umbilico-ileal fistulas
* Hirschsprung's disease
- *Definition:* Hirschsprung's disease is a congenital anomaly and a familial condition caused by migratory failure of neural crest cells leading to abnormal innervations of the bowel i.e absence of intramural ganglion cells (Myenteric Auerbachs's plexus and submucosal Meissner's plexus) more commonly in anorectum but can involve the entire intestine.
- Most common cause of functional lower intestinal obstruction in neonates
- Absense of these cells causes functional intestinal obstruction at the level of aganglionosis, as these cells are respnosible for normal peristalsis.
- In most cases the aganglionosis involves the rectum or rectosigmoid, but it can extend for varying lengths, and in 5% to 10% of cases can involve the entire colon or even a significant amount of the small intestine
- The defect begins in the internal anal sphincter and extends proximally for avariable length of gut
- *Types:*
  + Short segment: Aganglionosis restricted to the rectum and sigmoid colon (in 75% patient)
  + Long segment: Involves the proximal colon
- *Clinical features:*
  + Failure to pass meconium in the 1^st 24 hour of life
    - 98% of neonates pass meconium in the first 24 hours of age
    - Any newborn who fails to pass meconium in the first 24-48 hours of life should be evaluated for possible Hirschsprung's disease
    - Characterstic but only present in approximately 90% cases
  + Neonatal Intestinal Obstruction
    - Symptoms include
      + Bilious vomiting
      + Abdominal distension
      + Refusal to feed
  + Recurrent enterocloitis (HAEC/Hirschsprung-associated enterocolitis)
    - Mainly in the 1^st three months of life
    - Severe diarrhoea alternating with constipation
    - May be chronic or may be severe and life threatening
  + Spontaneous perforation
    - Occurs in 3%, especially in long segment aganglionosis
  + Chronic constipation
    - Some patients present later in childhood, or even during adulthood, with chronic constipation
    - This is most common among breast fed infants, who typically develop constipation around the time of weaning
    - May have growth retardation, multiple fecal masses on abdominal examination
  + Physical examination
    - Distended abdomen with multiple fecal masses on abdominal examination
    - DRE:
      + Anal sphincter is hypertonic
      + Rectum feels narrow and grip the finger (second grip)
      + Rectum is typically empty
      + Passing explosive stools after DRE with excessive gas
  + *Investigations:*
    - Plain Erect X ray abdomen: Intestinal obstruction
    - Barium enema: Extent of disease
    - Rectal biopsy: Gold standard, demonstrates:
      + Absence of ganglion cells in the submucosal and myenteric plexuses
      + Hypertrophied nerve trunks
* ARM
** Imperforate anus
- Most patients with imperforate anus have a fistulous connection with urethra in males or with vestibule in females
- *Clinical featurs:*
  + History of failure to pass meconium by 24 hours after birth
  + Absence of anal opening or visible anal pit
  + Abdominal distention
  + HIstory of passage of meconium through some abnormal openings either in the:
    - Perineum
    - Urethra
    - Vagina
- *Investigations:*
  + Invertogram at 24 hours:
    - The baby is held upside down and X-ray is focused from a lateral side
    - Reveals the level of gas shadow (rectal pouch)
  + Renal USG: to evaluate urinary tract abnormalities
* Intestinal perforation
- *Causes:*
  + Penetrating injury to the lower chest or abdomen
  + Blunt abdominal trauma to the stomach (Especially in the children)
  + Presence of a predisposing condition:
    - Peptic ulcer disease
    - Acute appendicitis
    - Acute diverticulitis
    - Inflamed Meckel diverticulum
  + Bacterial infections:
    - Typhoid fever
    - Tuberculosis
  + Ingestion of aspirin, NSAIDs and steroids
  + Intra-abdominal malignancy, lymphoma, metatstic renal carcinoma
  + Ingestion of caustic substances
  + Foreign body ingestion
* IHPS
- Infantile hypertrophic pyloric stenosis is a congenital gastric outlet obstruction occurring in the early infancy
- It results from diffuse hypertrophy and hyperplasia of the smooth muscles of the antrum of the stomach and pylorus
- *Cardinal features:*
  + Projectile non bilious vomiting, almost after every feed usually start around 3 weeks of age
  + The baby remains hungry and suchs vigorously after the episodes of vomiting
- *Signs:*
  + Visible peristalsis is seen passing from left to right of the abdomen and more obvious after a meal
  + The enlarged pylorus,classically described as an "Olive", may be palpated in the right upper quadrant of the abdomen or epigastrium
  + The baby may be dehydrated due to repeated vomiting
- *Investigations:*
  + USG of the abdomen:
    - Pyloric muscle thickness: >4mm
    - Diameter of pylorus: >11mm
    - Pyloric channel length: >17mm
  + Barium meal X-ray:
    - Shows an elongated pylorus
    - Double track sign
    - Shoulder sign
  + Serum electrolytes:
    - Hyponatraemia
    - Hypokalaemia
    - Hypochloraemia
    - Metabolic alkalosis
* Intussusception
- A segment of intestine invaginates into the adjoining intestinal lumen, causing bowel obstruction
- Mostly occuring around 6 months when complementary foods are introduced
- *Pathology:*
  + The proximal portion of bowel (the intussusceptum), invaginates into the distal portion of bowel (the intussuscipiens), pulling its mesentery along with it
  + In most cases, ileum invaginates into the caecum (ileo-colic variety)
- *Cardinal features:*
  + Intermittent, severe colicky abdominal pain in a previously well child
  + The child appears calm and relieved in between attacks
  + Non-bilious followed by bilious vomiting
  + Stool looks like red currant jelly (Red currant is a fruit, the stool looks like jelly made up from that fruit). This is a mixture of mucous, sloughed mucosa and blood
- *Signs:*
  + Abdomen is distended
  + A tender sausage shaped mass is felt in right hypochondrium and empty right lower quadrant (Dance sign)
  + Bowel sound may be absent
  + *DRE:*
    - Presence of bloody mucus on the finger as it is withdrawn
* Specimens
** Appendix
*Identification:* It is a pathological specimen of inflamed vermiform appendix.
- Worm like structure
- Mesoappendix is attached to it
- One end is blind and another end is open, which is ligated
- Appendix is swollen, red, oedematous and congested
- Tip is blackish: gangrene present (in case of gangrenous appendix)
*More liable to be gangrenous:*
- Tip of the appendix
- The site of obstruction
** Bladder stone
*Identification:*
| Type    | Phosphate     | Oxalate                    | Urate     | Cystine          |
|---------+---------------+----------------------------+-----------+------------------|
| Size    | Usually large | Usually small              | Small     | Small            |
| Shape   | Oval          | Rounded                    | Irregular | Amorphic         |
| Color   | Chalky white  | Blackish or deep chocolate | Yellowish | Yellow           |
| Surface | Smooth        | Spiky, not smooth          | Smooth    | Usually multiple |
- Primary:
  + Develops in apparently healthy urinary tract
  + In acidic urine
  + No nidus required
  + In sterile urine
  + Oxalate stone
  + Symptoms arise early
- Secondary:
  + Outflow obstruction
  + It is formed in the infected urine
  + In alkaline urine
  + There is obstruction in urinary flow
  + May occur in imparied bladder emptying
  + Phosphate stones are formed
  + May attain big size
  + Surface is usually smooth, hence symptoms are late
    Nidus requred (Bacteria, clumpt of epithelial tissue debris, small stone, foreign body)
- *Types:*
  + Mixed- Most common
  + Calcium Oxalate stones
    - Primary stones
    - Moderate size
    - Solitary
    - Uneven surface
    - Dark brown
  + Uric acid stones
    - Round to oval
    - Smooth
    - Yellow to brown
    - Radio-lucent
    - Occurs in patients with:
      + Gout
      + Ieostomies
      + Bladder outflow obstruction
  + Triple Phosphate stones
    - Composed of ammonium, magnesium and calcium phosphates
    - Occurs in infected urine with urea splitting organisms
    - Grows rapidly
    - Dirty white
    - Chalky in consistency
  + Cysteine stones
    - Presence of cystinuria
    - Radio-opaque
    - High sulphar content
- A bladder stone is usually free to move in the bladder
- May cause erosions and haematuria
- Gravitates to the lowest part of the bladder
- *Clinical features:*
  + Symptoms:
    - Frequency: Sensation of incomplete bladder emptying
    - Pain (strangury): Occurs at the end of micturition, refered to tip of penis, pain is worsened by movement
    - Haematuria: Drops of bright red blood at the end of micturition
    - Interrruption of the urinary stream: Stone blocking the internal meatus
    - UTI
  + Examination:
    - Per abdomen: Large calculus is palpable in the female in suprapubic region
- *Investigaions:*
  + Urine R/M/E:
    - Microscopic haematuria
    - Pus or crystals that are typical of the calculus
    - Ultrasound abdomen
    - X-ray KUB
** Worm
